﻿//
// Lazy.cs
//
// Authors:
//  Zoltan Varga (vargaz@gmail.com)
//  Marek Safar (marek.safar@gmail.com)
//
// Copyright (C) 2009 Novell
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// Modifications:
//  Updated for Bridge.NET
//
// Sublicense:
//
//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;

namespace System
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
	public class Lazy<T> 
	{
		T value;
		Func<T> factory;
		Exception exception;
		bool inited;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isThreadSafe"></param>
		public Lazy (bool isThreadSafe)
			: this (Activator.CreateInstance<T>)
		{
		}
		
        /// <summary>
        /// 
        /// </summary>
        /// <param name="valueFactory"></param>
        /// <param name="isThreadSafe"></param>
		public Lazy (Func<T> valueFactory, bool isThreadSafe)
			: this (valueFactory)
		{
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mode"></param>
		public Lazy ()
			: this (Activator.CreateInstance<T>)
		{
		}
		
        /// <summary>
        /// 
        /// </summary>
        /// <param name="valueFactory"></param>
        /// <param name="mode"></param>
		public Lazy (Func<T> valueFactory)
		{
			if (valueFactory == null)
				throw new ArgumentNullException ("valueFactory");
			this.factory = valueFactory;
		}

        /// <summary>
        /// 
        /// </summary>
		// Don't trigger expensive initialization
		public T Value {
			get {
				if (inited)
					return value;
				if (exception != null)
					throw exception;

				return InitValue ();
			}
		}

		T InitValue ()
		{
			Func<T> init_factory;
			T v;
			
			if (inited)
				return value;

			if (factory == null) {
				if (exception == null)
					throw new InvalidOperationException ("The initialization function tries to access Value on this instance");

				throw exception;
			}

			init_factory = factory;
			try {
				factory = null;
				v = init_factory ();
				value = v;
				inited = true;
			} catch (Exception ex) {
				exception = ex;
				throw;
			}

			return value;
		}

        /// <summary>
        /// 
        /// </summary>
		public bool IsValueCreated {
			get {
				return inited;
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
		public override string ToString ()
		{
			if (inited)
				return value.ToString ();
			else
				return "Value is not created";
		}
	}		
}
	