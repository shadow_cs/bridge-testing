﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Constraints;

namespace NUnit.Compatibility
{
    public static class Helpers
    {
        public static Bridge.Testing.TestStatus ToStatus(this ResultState state)
        {
            switch (state.Status)
            {
                case TestStatus.Inconclusive:
                    return Bridge.Testing.TestStatus.Inconclusive;
                case TestStatus.Skipped:
                    return Bridge.Testing.TestStatus.Skipped;
                case TestStatus.Passed:
                    return Bridge.Testing.TestStatus.Passed;
                case TestStatus.Warning:
                    return Bridge.Testing.TestStatus.Warning;
                case TestStatus.Failed:
                    return Bridge.Testing.TestStatus.Failed;
                default:
                    throw new ArgumentException(nameof(state));
            }
        }

        public static Bridge.Testing.TestStatus ToStatus(this AssertionStatus state)
        {
            switch (state)
            {
                case AssertionStatus.Inconclusive:
                    return Bridge.Testing.TestStatus.Inconclusive;
                case AssertionStatus.Passed:
                    return Bridge.Testing.TestStatus.Passed;
                case AssertionStatus.Warning:
                    return Bridge.Testing.TestStatus.Warning;
                case AssertionStatus.Failed:
                    return Bridge.Testing.TestStatus.Failed;
                case AssertionStatus.Error:
                    return Bridge.Testing.TestStatus.Error;
                default:
                    throw new ArgumentException(nameof(state));
            }
        }

        public static Bridge.Testing.TestStatus ToStatus(this ConstraintStatus status)
        {
            switch (status)
            {
                case ConstraintStatus.Unknown:
                    return Bridge.Testing.TestStatus.Inconclusive;
                case ConstraintStatus.Success:
                    return Bridge.Testing.TestStatus.Passed;
                case ConstraintStatus.Failure:
                    return Bridge.Testing.TestStatus.Warning;
                case ConstraintStatus.Error:
                    return Bridge.Testing.TestStatus.Failed;
                default:
                    throw new ArgumentException(nameof(status));
            }
        }

        public static bool EndsWith(this string s, string suffix, StringComparison info)
        {
            return s.EndsWith(suffix);
        }
    }
}
