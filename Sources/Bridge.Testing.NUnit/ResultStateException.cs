﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Bridge;
using Bridge.Testing;
using NUnit.Compatibility;
using NUnit.Framework.Interfaces;
using System;

namespace NUnit.Framework
{
    // Reflectable required for testing that needs metadata for Message.
    [Reflectable]
    public abstract class ResultStateException: TestStatusException
    {
        public override string Message => base.Message;
        
        public abstract ResultState ResultState { get; }

        protected ResultStateException() : base()
        {
        }

        public ResultStateException(string message) : base(message)
        {
        }

        public ResultStateException(string message, Exception inner) : base(message, inner)
        {
        }

        public override Bridge.Testing.TestStatus Status
        {
            get
            {
                return ResultState.ToStatus();
            }
        }
    }
}
