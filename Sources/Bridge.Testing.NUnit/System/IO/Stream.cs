﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

namespace System.IO
{
    public abstract class Stream : IDisposable
    {
        public abstract void Dispose();
        public abstract long Position { get; set; }
        public abstract long Length { get; }
        public bool CanRead { get; }
        public bool CanSeek { get; }

        public abstract void Seek(long v, SeekOrigin begin);
    }

    public enum SeekOrigin
    {
        Begin,
        Current,
        End
    }
}
