﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using System.Globalization;

namespace System.IO
{
    public abstract class TextWriter : IDisposable
    {
        protected TextWriter()
        {
            FormatProvider = CultureInfo.InvariantCulture;
        }
        //
        // Summary:
        //     Initializes a new instance of the System.IO.TextWriter class with the specified
        //     format provider.
        //
        // Parameters:
        //   formatProvider:
        //     An System.IFormatProvider object that controls formatting.
        protected TextWriter(IFormatProvider formatProvider)
        {
            FormatProvider = formatProvider;
        }

        public virtual IFormatProvider FormatProvider { get; }

        public void Dispose()
        {
        }

        public abstract void Write(string value);
        public abstract void Write(string format, params object[] args);
        public abstract void WriteLine();
        public abstract void WriteLine(string value);
        public abstract void WriteLine(string format, params object[] args);
    }
}
