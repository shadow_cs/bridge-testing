﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

namespace System.IO
{
    public class StringWriter : TextWriter
    {
        string buffer = "";

        public StringWriter() : base() { }

        public StringWriter(IFormatProvider formatProvider) : base(formatProvider) { }

        public override string ToString()
        {
            return buffer;
        }

        public override void Write(string value)
        {
            buffer += value;
        }

        public override void Write(string format, params object[] args)
        {
            buffer += String.Format(format, args);
        }

        public override void WriteLine()
        {
            buffer += Environment.NewLine;
        }

        public override void WriteLine(string value)
        {
            buffer += value + Environment.NewLine;
        }

        public override void WriteLine(string format, params object[] args)
        {
            buffer += String.Format(format, args) + Environment.NewLine;
        }
    }
}
