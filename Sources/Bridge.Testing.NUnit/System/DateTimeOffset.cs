﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using System.Globalization;

namespace System
{
    public struct DateTimeOffset
    {
        public TimeSpan Duration()
        {
            throw new NotImplementedException();
        }

        public TimeSpan Offset { get { throw new NotImplementedException(); } }

        public string ToString(string format, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public static DateTimeOffset operator -(DateTimeOffset left, DateTimeOffset right)
        {
            throw new NotImplementedException();
        }

        public static bool operator ==(DateTimeOffset left, DateTimeOffset right)
        {
            throw new NotImplementedException();
        }

        public static bool operator !=(DateTimeOffset left, DateTimeOffset right)
        {
            throw new NotImplementedException();
        }

        public override bool Equals(object o)
        {
            throw new NotImplementedException();
            return base.Equals(o);
        }

        public override int GetHashCode()
        {
            throw new NotImplementedException();
            return base.GetHashCode();
        }
    }
}
