﻿// ***********************************************************************
// Copyright (c) 2014 Charlie Poole
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ***********************************************************************
//
// Modifications:
//  Updated for Bridge.NET
//
// Sublicense:
//
//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using System;

namespace NUnit.Framework
{
    public abstract class NUnitAttribute : Attribute
    {
        public string Author { get; set; }
        public string Description { get; set; }
    }

    public class AuthorAttribute : NUnitAttribute
    {
        public string Mail { get; private set; }

        public AuthorAttribute(string author, string mail = "")
        {
            Author = author;
            Mail = mail;
        }
    }

    public class DescriptionAttribute : NUnitAttribute
    {
        public DescriptionAttribute(string description)
        {
            Description = description;
        }
    }

    /// <summary>
    /// Adding this attribute to a method within a <seealso cref="TestFixtureAttribute"/> 
    /// class makes the method callable from the NUnit test runner. There is a property 
    /// called Description which is optional which you can provide a more detailed test
    /// description. This class cannot be inherited.
    /// </summary>
    /// 
    /// <example>
    /// [TestFixture]
    /// public class Fixture
    /// {
    ///   [Test]
    ///   public void MethodToTest()
    ///   {}
    ///   
    ///   [Test(Description = "more detailed description")]
    ///   public void TestDescriptionMethod()
    ///   {}
    /// }
    /// </example>
    /// 
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class TestAttribute : NUnitAttribute
    {
    }

    /// <summary>
    /// TestFixtureAttribute is used to mark a class that represents a TestFixture.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public sealed class TestFixtureAttribute : NUnitAttribute
    { }

    /// <summary>
    /// Attribute used to identify a method that is called once
    /// to perform setup before any child tests are run.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class OneTimeSetUpAttribute : NUnitAttribute
    { }

    /// <summary>
    /// Attribute used to identify a method that is called once
    /// after all the child tests have run. The method is 
    /// guaranteed to be called, even if an exception is thrown.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class OneTimeTearDownAttribute : NUnitAttribute
    { }

    /// <summary>
    /// Attribute used to mark a class that contains one-time SetUp 
    /// and/or TearDown methods that apply to all the tests in a
    /// namespace or an assembly.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class SetUpAttribute : NUnitAttribute
    { }

    /// <summary>
    /// Attribute used to identify a method that is called 
    /// immediately after each test is run. The method is 
    /// guaranteed to be called, even if an exception is thrown.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class TearDownAttribute : NUnitAttribute
    { }

    /// <summary>
    /// ParallelizableAttribute is used to mark tests that may be run in parallel.
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public sealed class ParallelizableAttribute : NUnitAttribute
    {
        public ParallelizableAttribute(ParallelScope scope) { }
    }

    /// <summary>
    /// Attribute used to apply a category to a test
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Assembly, AllowMultiple = true, Inherited = true)]
    public class Category : NUnitAttribute
    {
        public Category(string name) { }
    }
}
