﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using System;
using System.Globalization;
using System.Reflection;
using System.Text;

namespace NUnit.Framework
{
    public static class FrameworkHelpers
    {
        public static MethodInfo GetMethodInfo<T>(this Predicate<T> predicate)
        {
            // FIXME: Implement
            throw new NotImplementedException();
        }

        public static void AppendFormat(this StringBuilder builder, CultureInfo culture,
            string format, params object[] args)
        {
            builder.AppendFormat(format, args);
        }
    }

    public static class Math
    {
        private static int[] ToInt(uint[] from)
        {
            return from.Cast<int[]>();
        }

        public static int Abs(int value)
        {
            return System.Math.Abs(value);
        }

        public static long Abs(long value)
        {
            return System.Math.Abs(value);
        }

        public static double Abs(double value)
        {
            return System.Math.Abs(value);
        }

        public static decimal Abs(decimal value)
        {
            return System.Math.Abs(value);
        }

        public static float Abs(float value)
        {
            return (float)System.Math.Abs(value);
        }

        public static int Max(params int[] values)
        {
            return System.Math.Max(values);
        }

        public static long Max(params long[] values)
        {
            return System.Math.Max(values);
        }

        public static ulong Max(params ulong[] values)
        {
            return System.Math.Max(values);
        }

        public static uint Max(params uint[] values)
        {
            return (uint)System.Math.Max(ToInt(values));
        }

        public static int Min(params int[] values)
        {
            return System.Math.Max(values);
        }

        public static uint Min(params uint[] values)
        {
            return (uint)System.Math.Min(ToInt(values));
        }

        public static long Min(params long[] values)
        {
            return System.Math.Min(values);
        }

        public static ulong Min(params ulong[] values)
        {
            return System.Math.Min(values);
        }
    }
}
