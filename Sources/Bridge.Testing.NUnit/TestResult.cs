﻿// ***********************************************************************
// Copyright (c) 2010-2014 Charlie Poole
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
// ***********************************************************************
// Modifications:
//  Updated for Bridge.NET
//
// Sublicense:
//
//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework.Interfaces;
using NUnit.Compatibility;
using Bridge.Testing;
using NUnit.Framework.Constraints;
using System;

namespace NUnit.Framework.Internal
{
    public abstract class TestResult: AssertBase, ITestResult
    {
        /// <summary>
        /// Error message for when child tests have errors
        /// </summary>
        internal static readonly string CHILD_ERRORS_MESSAGE = "One or more child tests had errors";

        /// <summary>
        /// Error message for when child tests have warnings
        /// </summary>
        internal static readonly string CHILD_WARNINGS_MESSAGE = "One or more child tests had warnings";

        /// <summary>
        /// Error message for when child tests are ignored
        /// </summary>
        internal static readonly string CHILD_IGNORE_MESSAGE = "One or more child tests were ignored";

        /// <summary>
        /// The minimum duration for tests
        /// </summary>
        internal const double MIN_DURATION = 0.000001d;

        private List<AssertionResult> assertionResults = new List<AssertionResult>();
        
        public int AssertCount { get; internal set; }

        public IList<AssertionResult> AssertionResults => assertionResults;

        public abstract IEnumerable<ITestResult> Children { get; }

        public string FullName => Test.ClassName + '.' + Name;

        public abstract bool HasChildren { get; }

        public string Message { get; private set; }

        public string Name => Test.Name;

        public StringWriter OutWriter { get; private set; }

        public string Output => OutWriter.ToString();

        /// <summary>
        /// Gets a count of pending failures (from Multiple Assert)
        /// </summary>
        public int PendingFailures => AssertionCount(AssertionStatus.Failed);

        public ResultState ResultState { get; private set; }

        public string StackTrace { get; private set; }

        public ITest Test { get; }

        /// <summary>
        /// Gets the worst assertion status (highest enum) in all the assertion results
        /// </summary>
        public AssertionStatus WorstAssertionStatus => 
            assertionResults.Aggregate((ar1, ar2) => ar1.Status > ar2.Status ? ar1 : ar2).Status;
        
        public double Duration { get { throw new NotImplementedException(); } }

        public DateTime StartTime { get { throw new NotImplementedException(); } }

        public DateTime EndTime { get { throw new NotImplementedException(); } }

        public abstract int FailCount { get; }

        public abstract int WarningCount { get; }

        public abstract int PassCount { get; }

        public abstract int SkipCount { get; }

        public abstract int InconclusiveCount { get; }

        public TestResult(ITest test)
        {
            OutWriter = new StringWriter();
            Test = test;
            ResultState = ResultState.Inconclusive;
        }

        /// <summary>
        /// Set the test result based on the type of exception thrown
        /// </summary>
        /// <param name="ex">The exception that was thrown</param>
        public void RecordException(Exception ex)
        {
            if (ex is ResultStateException)
            {
                string message = ex is MultipleAssertException
                    ? CreateLegacyFailureMessage()
                    : ex.Message;

                string stackTrace = StackFilter.DefaultFilter.Filter(ex.StackTrace);

                SetResult(((ResultStateException)ex).ResultState, message, stackTrace);
            }
            else
            {
                string message = ExceptionHelper.BuildMessage(ex);
                string stackTrace = ExceptionHelper.BuildStackTrace(ex);
                SetResult(ResultState.Error, message, stackTrace);

                if (AssertionResults.Count > 0)
                {
                    // Add pending failures to the legacy result message
                    Message += CreateLegacyFailureMessage();

                    // Add to the list of assertion errors, so that newer runners will see it
                    AssertionResults.Add(new AssertionResult(AssertionStatus.Error, message, stackTrace));
                }
            }
        }

        /// <summary>
        /// Set the test result based on the type of exception thrown
        /// </summary>
        /// <param name="ex">The exception that was thrown</param>
        /// <param name="site">The FailureSite to use in the result</param>
        public void RecordException(Exception ex, FailureSite site)
        {
            if (ex is ResultStateException)
                SetResult(((ResultStateException)ex).ResultState.WithSite(site),
                    ex.Message,
                    StackFilter.DefaultFilter.Filter(ex.StackTrace));
            else
                SetResult(ResultState.Error.WithSite(site),
                    ExceptionHelper.BuildMessage(ex),
                    ExceptionHelper.BuildStackTrace(ex));
        }

        public void RecordSetUpException(Exception ex)
        {
            ResultState resultState = ResultState == ResultState.Cancelled
                ? ResultState.Cancelled
                : ResultState.Error;
            if (Test.IsSuite)
                resultState = resultState.WithSite(FailureSite.SetUp);

            string message = "SetUp : " + ExceptionHelper.BuildMessage(ex);
            if (Message != null)
                message = Message + Environment.NewLine + message;

            string stackTrace = "--SetUp" + Environment.NewLine + ExceptionHelper.BuildStackTrace(ex);
            if (StackTrace != null)
                stackTrace = StackTrace + Environment.NewLine + stackTrace;

            SetResult(resultState, message, stackTrace);
        }

        /// <summary>
        /// RecordTearDownException appends the message and stacktrace
        /// from an exception arising during teardown of the test
        /// to any previously recorded information, so that any
        /// earlier failure information is not lost. Note that
        /// calling Assert.Ignore, Assert.Inconclusive, etc. during
        /// teardown is treated as an error. If the current result
        /// represents a suite, it may show a teardown error even
        /// though all contained tests passed.
        /// </summary>
        /// <param name="ex">The Exception to be recorded</param>
        public void RecordTearDownException(Exception ex)
        {
            ResultState resultState = ResultState == ResultState.Cancelled
                ? ResultState.Cancelled
                : ResultState.Error;
            if (Test.IsSuite)
                resultState = resultState.WithSite(FailureSite.TearDown);

            string message = "TearDown : " + ExceptionHelper.BuildMessage(ex);
            if (Message != null)
                message = Message + Environment.NewLine + message;

            string stackTrace = "--TearDown" + Environment.NewLine + ExceptionHelper.BuildStackTrace(ex);
            if (StackTrace != null)
                stackTrace = StackTrace + Environment.NewLine + stackTrace;

            SetResult(resultState, message, stackTrace);
        }

        /// <summary>
        /// Determine result after test has run to completion.
        /// </summary>
        public void RecordTestCompletion()
        {
            switch (AssertionResults.Count)
            {
                case 0:
                    SetResult(ResultState.Success);
                    break;
                case 1:
                    SetResult(
                        AssertionStatusToResultState(AssertionResults[0].Status),
                        AssertionResults[0].Message,
                        AssertionResults[0].StackTrace);
                    break;
                default:
                    SetResult(
                        AssertionStatusToResultState(WorstAssertionStatus),
                        CreateLegacyFailureMessage());
                    break;
            }
        }

        public void RecordAssertion(ConstraintResult result, AssertionResult assertion)
        {
            assertionResults.Add(assertion);
            object expected = null;
            object actual = null;
            if (result != null)
            {
                actual = result.ActualValue;
                dynamic res = result;
                // This may return undefined but should not fail with null exception
                expected = res.expectedValue || res._constraint.expected;
            }
            Asserter.Push(assertion.Status.ToStatus(), expected, actual, assertion.Message);
        }

        /// <summary>
        /// Record an assertion result
        /// </summary>
        public void RecordAssertion(ConstraintResult result, AssertionStatus status, string message, string stackTrace)
        {
            RecordAssertion(result, new AssertionResult(status, message, stackTrace));
        }

        /// <summary>
        /// Record an assertion result
        /// </summary>
        public void RecordAssertion(ConstraintResult result, AssertionStatus status, string message)
        {
            RecordAssertion(result, status, message, null);
        }

        /// <summary>
        /// Set the result of the test
        /// </summary>
        /// <param name="resultState">The ResultState to use in the result</param>
        public void SetResult(ResultState resultState)
        {
            SetResult(resultState, null, null);
        }

        /// <summary>
        /// Set the result of the test
        /// </summary>
        /// <param name="resultState">The ResultState to use in the result</param>
        /// <param name="message">A message associated with the result state</param>
        public void SetResult(ResultState resultState, string message)
        {
            SetResult(resultState, message, null);
        }

        /// <summary>
        /// Set the result of the test
        /// </summary>
        /// <param name="resultState">The ResultState to use in the result</param>
        /// <param name="message">A message associated with the result state</param>
        /// <param name="stackTrace">Stack trace giving the location of the command</param>
        public void SetResult(ResultState resultState, string message, string stackTrace)
        {
            ResultState = resultState;
            Message = message;
            StackTrace = stackTrace;
        }

        private int AssertionCount(AssertionStatus status)
        {
            return assertionResults.Count(ar => ar.Status == status);
        }

        private ResultState AssertionStatusToResultState(AssertionStatus status)
        {
            switch (status)
            {
                case AssertionStatus.Inconclusive:
                    return ResultState.Inconclusive;
                default:
                case AssertionStatus.Passed:
                    return ResultState.Success;
                case AssertionStatus.Warning:
                    return ResultState.Warning;
                case AssertionStatus.Failed:
                    return ResultState.Failure;
                case AssertionStatus.Error:
                    return ResultState.Error;
            }
        }

        /// <summary>
        /// Creates a failure message incorporating failures
        /// from a Multiple Assert block for use by runners
        /// that don't know about AssertionResults.
        /// </summary>
        /// <returns>Message as a string</returns>
        private string CreateLegacyFailureMessage()
        {
            var writer = new StringWriter();

            writer.WriteLine("\n  One or more failures in Multiple Assert block:");

            int counter = 0;
            foreach (var assertion in AssertionResults)
                writer.WriteLine(string.Format("  {0}) {1}", ++counter, assertion.Message));

            return writer.ToString();
        }        
    }
}
