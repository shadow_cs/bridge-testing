﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Reflection;
using Bridge.Testing.Helpers;
using NUnit.Framework.Internal;
using NUnit.Framework.Interfaces;
using System.Diagnostics;

namespace Bridge.Testing.NUnit
{
    public class NUnitTestCase : ITestCase
    {
        private IEnumerable<NUnitTestMethod> methods = null;
        private TestSuite suite = null;
        private bool providedInstance = false;
        internal object instance = null;
        internal TestSuiteResult lastResult = null;
        internal MethodInfo[] setup = null;
        internal MethodInfo[] teardown = null;
        public Type Type { get; }
        public ITestResult LastResult => lastResult;

        public IEnumerable<ITestMethod> Methods
        {
            get
            {
                if (methods == null)
                    InitializeMethods();
                return methods;
            }
        }

        public NUnitTestCase(Type type)
        {
            Type = type;
        }

        public NUnitTestCase(object fixture)
        {
            if (fixture is Type)
                throw new ArgumentException($"{nameof(fixture)} must not be of type Type");
            Type = fixture.GetType();
            providedInstance = true;
            instance = fixture;
        }

        public void AddMethod(string name)
        {
            Debug.Assert(methods == null);
            methods = new List<NUnitTestMethod>
            {
                new NUnitTestMethod(this, Type.GetMethod(name))
            };
        }

        private void InitializeMethods()
        {
            var result = new List<NUnitTestMethod>();
            foreach (var method in Type.GetMethods())
            {
                if (method.HasAttribute<TestAttribute>(true))
                    result.Add(new NUnitTestMethod(this, method));
            }
            methods = result;
        }

        public void Begin()
        {
            // FIXME: Base classes first for setup last for teardown (see BaseSetUpIsCalledFirstTearDownLast test)
            setup = Type.GetMethods().Filter(m => m.HasAttribute<SetUpAttribute>(true));
            teardown = Type.GetMethods().Filter(m => m.HasAttribute<TearDownAttribute>(true));
            if (!providedInstance)
                instance = Activator.CreateInstance(Type);
            suite = new TestSuite(Type.Name, "");
            lastResult = (TestSuiteResult)suite.MakeTestResult();
            using (new TestExecutionContext.IsolatedContext())
            {
                var context = TestExecutionContext.CurrentContext;
                context.CurrentTest = suite;
                context.CurrentResult = lastResult;
                using (new AssertBase.NullAssert())
                {
                    try
                    {
                        foreach (var m in Type.GetMethods().Filter(m => m.HasAttribute<OneTimeSetUpAttribute>(true)))
                        {
                            m.Invoke(instance);
                        }
                    }
                    catch (Exception e)
                    {
                        lastResult.RecordException(e, FailureSite.SetUp);
                        throw;
                    }
                }
            }
        }

        public void End()
        {
            // Error during class activation
            if (instance == null)
                return;
            try
            {
                using (new TestExecutionContext.IsolatedContext())
                {
                    var context = TestExecutionContext.CurrentContext;
                    context.CurrentTest = suite;
                    context.CurrentResult = lastResult;
                    using (new AssertBase.NullAssert())
                    {
                        try
                        {
                            foreach (var m in Type.GetMethods().Filter(m => m.HasAttribute<OneTimeTearDownAttribute>(true)))
                            {
                                m.Invoke(instance);
                            }
                        }
                        catch (Exception e)
                        {
                            lastResult.RecordTearDownException(e);
                            throw;
                        }
                    }
                }
            }
            finally
            {
                if (!providedInstance)
                    instance = null;
            }
        }
    }

    public class NUnitTestMethod : TestMethodBase
    {
        private readonly NUnitTestCase owner;
        private TestResult lastResult = null;

        public override MethodInfo Method { get; }

        public ITestResult LastResult => lastResult;

        public NUnitTestMethod(NUnitTestCase owner, MethodInfo method)
        {
            Method = method;
            this.owner = owner;
        }

        public override void Run(IAsserter asserter)
        {
            RunInternal(asserter, true);
        }

        public void RunInternal(IAsserter asserter, bool isolated)
        {
            using (new TestExecutionContext.IsolatedContext())
            {
                var context = TestExecutionContext.CurrentContext;
                var test = context.CurrentTest = new TestMethod(owner.Type.Name, Method.Name);
                lastResult = context.CurrentResult = test.MakeTestResult();
                IDisposable isolation;
                if (isolated)
                    isolation = new AssertBase.IsolatedAssert(asserter);
                else isolation = new AssertBase.NestedAssert(asserter);
                using (isolation)
                {
                    try
                    {
                        RunMethod();
                    }
                    finally
                    {
                        owner.lastResult.AddResult(lastResult);
                    }
                }
            }
        }

        private void RunMethod()
        {
            try
            {
                // Run set up
                foreach (var m in owner.setup)
                    m.Invoke(owner.instance);
                // Run test method
                Invoke(owner.instance, Method);
                lastResult.RecordTestCompletion();
            }
            catch (Exception e1)
            {
                lastResult.RecordException(e1);
                throw;
            }
            finally
            {
                try
                {
                    foreach (var m in owner.teardown)
                        m.Invoke(owner.instance);
                }
                catch (Exception e2)
                {
                    lastResult.RecordTearDownException(e2);
                    throw;
                }
            }
        }
    }

    public class NUnitHarvester : TestHarvester
    {
        public override IEnumerable<ITestCase> Harvest(Assembly assembly)
        {
            var result = new List<ITestCase>();
            foreach (var type in assembly.GetTypes())
            {
                if (type.IsClass && type.HasAttribute<TestFixtureAttribute>(true))
                    result.Add(new NUnitTestCase(type));
            };
            return result;
        }
    }
}
