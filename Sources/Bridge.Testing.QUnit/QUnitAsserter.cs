﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using System;

namespace Bridge.Testing.QUnit
{
    [External]
    abstract class QUnit2Assert : Bridge.QUnit.Assert
    {
        public abstract void PushResult(object result);
    }

    class QUnitAsserter: AsserterBase
    {
        internal QUnit2Assert Assert;

        public override bool CheckTrue(bool value, string message)
        {
            Mark(value);
            Assert.Ok(value, message);
            return value;
        }

        public override bool CheckFalse(bool value, string message)
        {
            Mark(!value);
            Assert.NotOk(value, message);
            return !value;
        }

        public override bool Equal(object expected, object actual, string message)
        {
            bool result = IsEqual(expected, actual);
            if (IsNative(expected) && IsNative(actual))
                Assert.StrictEqual(actual, expected, message);
            else Assert.Ok(result);
            return result;
        }

        public override void Fail(string message = null)
        {
            Mark(TestStatus.Failed);
            Assert.Ok(false, message);
        }

        public override bool NotEqual(object expected, object actual, string message)
        {
            bool result = IsEqual(expected, actual);
            if (IsNative(expected) && IsNative(actual))
                Assert.NotStrictEqual(actual, expected, message);
            else Assert.NotOk(result); 
            return !result;
        }

        public override void Pass(string message)
        {
            if (Status == TestStatus.NoCheck)
            {
                Mark(TestStatus.Passed);
                Assert.Expect(0);
            }
        }

        public override void Inconclusive(string message)
        {
            // Do not call mark, treat the test as if there were no assertions.
            Mark(TestStatus.Inconclusive);
            PushResult(false, null, null, message);
        }

        public override bool Push(TestStatus result, object expected, object actual, string message)
        {
            Mark(result);
            var b = result == TestStatus.Passed;
            PushResult(b, expected, actual, message);
            return b;
        }

        private void PushResult(bool result, object actual, object expected, string message)
        {
            Assert.PushResult(new
            {
                Result = result,
                Actual = actual,
                Expected = expected,
                Message = message
            });
        }
    }
}
