﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using Bridge.Html5;

namespace Bridge.Testing.QUnit
{
    using QU = Bridge.QUnit.QUnit;

    // Bridge.QUnit is missing those
    [External]
    [ObjectLiteral]
    public class ModuleHooks: Bridge.QUnit.ModuleHooks
    {
        public ModuleHooks() { }

        public Action After { get; set; }
        public Action Before { get; set; }
    }

    public class QUnitRunner: TestRunner
    {
        private static QUnitRunner instance = null;

        public static QUnitRunner Instance
        {
            get
            {
                if (instance == null)
                    instance = new QUnitRunner();
                return instance;
            }
        }

        private bool done = false;

        private QUnitRunner()
        {

        }

        public bool FailIfNoCheck { get; set; } = true;

        private void RunTestCase(ITestCase testCase, QUnitAsserter asserter)
        {
            bool oneTimeSetupFailed = false;
            // QUnit runs after even if before throws an exception
            var hooks = new ModuleHooks()
            {
                Before = () =>
                {
                    try
                    {
                        testCase.Begin();
                    }
                    catch
                    {
                        oneTimeSetupFailed = true;
                        throw;
                    }
                },
                After = testCase.End
            };
            QU.Module(testCase.Type.FullName, hooks);

            foreach (var test in testCase.Methods)
            {
                QU.Test(test.Method.Name, (a) =>
                {
                    asserter.Assert = a.As<QUnit2Assert>();
                    try
                    {
                        if (oneTimeSetupFailed)
                        {
                            asserter.Fail("One time setup failed");
                            return;
                        }
                        test.Run(asserter);
                        // Be default QUnit is expecting at least one assertion, but most .Net testing frameworks
                        // pass without an assertion being called.
                        if ((!asserter.CheckCalled) && (!FailIfNoCheck))
                            a.Expect(0);
                    }
                    catch (TestStatusException e)
                    {
                        // Some frameworks require the test to stop on first error
                        // since QUnit chooses to continue the test method
                        // on failure, the asserter may enforce the stop by throwing
                        // this exception (this should be configurable to let the
                        // user choose).
                        // In case someone else throws the error and not our own 
                        // asserter we have to raise the exception either way.
                        // Or just record the result if not already recorded by someone 
                        // else with the same or higher status.
                        if (e.Status >= TestStatus.Error)
                            throw e;
                        if (asserter.Status < e.Status)
                            asserter.Push(e.Status, null, null, e.Message);
                    }
                    finally
                    {
                        asserter.ResetStatus();
                    }
                });
            }
        }

        public override void RunTests(IEnumerable<ITestCase> tests)
        {
            if (done)
                throw new NotSupportedException("Cannot run tests twice");
            done = true;

            if (Document.GetElementById("qunit") == null)
                Document.Body.AppendChild(new HTMLDivElement() { Id = "qunit" });
            if (Document.GetElementById("qunit-fixture") == null)
                Document.Body.AppendChild(new HTMLDivElement() { Id = "qunit-fixture" });

            var asserter = new QUnitAsserter();
            foreach (var testCase in tests)
            {
                RunTestCase(testCase, asserter);
            }
            if (! QU.Config.Autostart)
                QU.Start();
        }
    }
}
