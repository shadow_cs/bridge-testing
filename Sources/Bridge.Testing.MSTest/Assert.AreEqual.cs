﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Bridge.Testing;
using System;

namespace Microsoft.VisualStudio.TestTools.UnitTesting
{
    public partial class Assert : AssertBase
    {
        private static void AreEqual_(object expected, object actual, string message)
        {
            PostCheck(Asserter.Equal(expected, actual, message), ExceptionFactory, message);
        }

        public static void AreEqual(object expected, object actual)
        {
            AreEqual_(expected, actual, null);
        }

        public static void AreEqual(string expected, string actual, bool ignoreCase)
        {
            AreEqual(expected, actual, ignoreCase, null);
        }

        public static void AreEqual(double expected, double actual, double delta)
        {
            AreEqual(expected, actual, delta, null);
        }

        public static void AreEqual(float expected, float actual, float delta)
        {
            AreEqual(expected, actual, delta, null);
        }

        public static void AreEqual(object expected, object actual, string message)
        {
            AreEqual_(expected, actual, message);
        }

        public static void AreEqual(string expected, string actual, bool ignoreCase,
            string message)
        {
            if (ignoreCase)
            {
                expected = expected.ToLower();
                actual = actual.ToLower();
            }
            PostCheck(Asserter.Equal(expected, actual, message), ExceptionFactory, message);
        }

        public static void AreEqual(float expected, float actual, float delta, string message)
        {
            var diff = Math.Abs(expected - actual);
            PostCheck(Asserter.CheckTrue(diff <= delta, message), ExceptionFactory, message);
        }

        public static void AreEqual(object expected, object actual, string message, params object[] parameters)
        {
            AreEqual_(expected, actual, Format(message, parameters));
        }

        public static void AreEqual(double expected, double actual, double delta, string message)
        {
            var diff = Math.Abs(expected - actual);
            PostCheck(Asserter.CheckTrue(diff <= delta, message), ExceptionFactory, message);
        }

        public static void AreEqual(string expected, string actual, bool ignoreCase, string message, params object[] parameters)
        {
            AreEqual(expected, actual, ignoreCase, Format(message, parameters));
        }

        public static void AreEqual(double expected, double actual, double delta, string message, params object[] parameters)
        {
            AreEqual(expected, actual, delta, Format(message, parameters));
        }

        public static void AreEqual(float expected, float actual, float delta, string message, params object[] parameters)
        {
            AreEqual(expected, actual, delta, Format(message, parameters));
        }

        public static void AreEqual<T>(T expected, T actual)
        {
            AreEqual_(expected, actual, null);
        }

        public static void AreEqual<T>(T expected, T actual, string message)
        {
            AreEqual_(expected, actual, message);
        }

        public static void AreEqual<T>(T expected, T actual, string message, params object[] parameters)
        {
            AreEqual_(expected, actual, Format(message, parameters));
        }
    }
}
