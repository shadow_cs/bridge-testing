﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Bridge.Testing;

namespace Microsoft.VisualStudio.TestTools.UnitTesting
{
    public partial class Assert : AssertBase
    {
        public static void AreNotSame(object notExpected, object actual)
        {
            AreNotSame(notExpected, actual, null);
        }

        public static void AreNotSame(object notExpected, object actual, string message)
        {
            PostCheck(Asserter.CheckFalse(notExpected == actual, message), ExceptionFactory, message);
        }

        public static void AreNotSame(object notExpected, object actual, string message, params object[] parameters)
        {
            AreNotSame(notExpected, actual, Format(message, parameters));
        }

        public static void AreSame(object expected, object actual)
        {
            AreSame(expected, actual, null);
        }

        public static void AreSame(object expected, object actual, string message)
        {
            PostCheck(Asserter.CheckTrue(expected == actual, message), ExceptionFactory, message);
        }

        public static void AreSame(object expected, object actual, string message, params object[] parameters)
        {
            AreSame(expected, actual, Format(message, parameters));
        }
    }
}
