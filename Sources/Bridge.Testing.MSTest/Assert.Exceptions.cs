﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Bridge.Testing;
using System;

namespace Microsoft.VisualStudio.TestTools.UnitTesting
{
    public partial class Assert : AssertBase
    {
        public static T ThrowsException<T>(Action action) where T : Exception
        {
            return ThrowsException<T>(action, null);
        }

        public static T ThrowsException<T>(Func<object> action) where T : Exception
        {
            return ThrowsException<T>(() => { action(); }, null);
        }

        public static T ThrowsException<T>(Action action, string message) where T : Exception
        {
            try
            {
                action();
            }
            catch (T e)
            {
                Asserter.CheckTrue(true, message);
                return e;
            }
            catch (Exception e)
            {
                Fail(message ?? $"Expected exception {typeof(T).Name} but {e.GetType().Name} was caught");
                return null;
            }
            Fail(message ?? $"Expected exception {typeof(T).Name}");
            return null;
        }

        public static T ThrowsException<T>(Func<object> action, string message) where T : Exception
        {
            return ThrowsException<T>(() => { action(); }, message);
        }

        public static T ThrowsException<T>(Action action, string message, params object[] parameters) where T : Exception
        {
            return ThrowsException<T>(action, Format(message, parameters));
        }

        public static T ThrowsException<T>(Func<object> action, string message, params object[] parameters) where T : Exception
        {
            return ThrowsException<T>(() => { action(); }, Format(message, parameters));
        }
    }
}
