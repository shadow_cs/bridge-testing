﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Bridge.Testing;
using System;

namespace Microsoft.VisualStudio.TestTools.UnitTesting
{
    public partial class Assert : AssertBase
    {
        public static void IsTrue(bool condition)
        {
            IsTrue(condition, null);
        }

        public static void IsTrue(bool condition, string message)
        {
            PostCheck(Asserter.CheckTrue(condition, message), ExceptionFactory, message);
        }

        public static void IsTrue(bool condition, string message, params object[] parameters)
        {
            IsTrue(condition, Format(message, parameters));
        }

        public static void IsFalse(bool condition)
        {
            IsFalse(condition, null);
        }

        public static void IsFalse(bool condition, string message)
        {
            PostCheck(Asserter.CheckFalse(condition, message), ExceptionFactory, message);
        }

        public static void IsFalse(bool condition, string message, params object[] parameters)
        {
            IsFalse(condition, Format(message, parameters));
        }

        public static void IsInstanceOfType(object value, Type expectedType)
        {
            IsInstanceOfType(value, expectedType, null);
        }

        public static void IsInstanceOfType(object value, Type expectedType, string message)
        {
            if (expectedType == null)
                throw new ArgumentNullException();
            else if (value != null)
                PostCheck(Asserter.CheckTrue(expectedType.IsAssignableFrom(value.GetType()), message),
                    ExceptionFactory, message);
            else PostCheck(Asserter.CheckTrue(false, message), ExceptionFactory, message);
        }

        public static void IsInstanceOfType(object value, Type expectedType, string message, params object[] parameters)
        {
            IsInstanceOfType(value, expectedType, Format(message, parameters));
        }

        public static void IsNotInstanceOfType(object value, Type wrongType)
        {
            IsNotInstanceOfType(value, wrongType, null);
        }

        public static void IsNotInstanceOfType(object value, Type wrongType, string message)
        {
            if (wrongType == null)
                throw new ArgumentNullException();
            else if (value != null)
                PostCheck(Asserter.CheckFalse(wrongType.IsAssignableFrom(value.GetType()), message),
                    ExceptionFactory, message);
            else PostCheck(Asserter.CheckTrue(false, message), ExceptionFactory, message);
        }

        public static void IsNotInstanceOfType(object value, Type wrongType, string message, params object[] parameters)
        {
            IsNotInstanceOfType(value, wrongType, Format(message, parameters));
        }

        public static void IsNotNull(object value)
        {
            IsNotNull(value, null);
        }

        public static void IsNotNull(object value, string message)
        {
            PostCheck(Asserter.CheckFalse(value == null, message), ExceptionFactory, message);
        }

        public static void IsNotNull(object value, string message, params object[] parameters)
        {
            IsNotNull(value, Format(message, parameters));
        }

        public static void IsNull(object value)
        {
            IsNull(value, null);
        }

        public static void IsNull(object value, string message)
        {
            PostCheck(Asserter.CheckTrue(value == null, message), ExceptionFactory, message);
        }

        public static void IsNull(object value, string message, params object[] parameters)
        {
            IsNull(value, Format(message, parameters));
        }
    }
}
