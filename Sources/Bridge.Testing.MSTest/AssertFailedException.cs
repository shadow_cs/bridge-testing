﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Bridge.Testing;
using System;

namespace Microsoft.VisualStudio.TestTools.UnitTesting
{
    public abstract class UnitTestAssertException : TestStatusException
    {
        protected UnitTestAssertException() : base() { }
        protected UnitTestAssertException(string msg) : base(msg) { }
        protected UnitTestAssertException(string msg, Exception ex) : base(msg, ex) { }
    }

    public class AssertFailedException : UnitTestAssertException
    {
        public AssertFailedException(): base() { }
        public AssertFailedException(string msg) : base(msg) { }
        public AssertFailedException(string msg, Exception ex) : base(msg, ex) { }

        public override TestStatus Status => TestStatus.Failed;
    }
}
