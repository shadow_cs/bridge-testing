﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Bridge.Testing;
using System;

namespace Microsoft.VisualStudio.TestTools.UnitTesting
{
    public partial class Assert : AssertBase
    {
        private static void AreNotEqual_(object expected, object actual, string message)
        {
            PostCheck(Asserter.NotEqual(expected, actual, message), ExceptionFactory, message);
        }

        public static void AreNotEqual(object expected, object actual)
        {
            AreNotEqual_(expected, actual, null);
        }

        public static void AreNotEqual(string expected, string actual, bool ignoreCase)
        {
            AreNotEqual(expected, actual, ignoreCase, null);
        }

        public static void AreNotEqual(double expected, double actual, double delta)
        {
            AreNotEqual(expected, actual, delta, null);
        }

        public static void AreNotEqual(float expected, float actual, float delta)
        {
            AreNotEqual(expected, actual, delta, null);
        }

        public static void AreNotEqual(object expected, object actual, string message)
        {
            AreNotEqual_(expected, actual, message);
        }

        public static void AreNotEqual(string expected, string actual, bool ignoreCase,
            string message)
        {
            if (ignoreCase)
            {
                expected = expected.ToLower();
                actual = actual.ToLower();
            }
            PostCheck(Asserter.NotEqual(expected, actual, message), ExceptionFactory, message);
        }

        public static void AreNotEqual(float expected, float actual, float delta, string message)
        {
            var diff = Math.Abs(expected - actual);
            PostCheck(Asserter.CheckFalse(diff <= delta, message), ExceptionFactory, message);
        }

        public static void AreNotEqual(object expected, object actual, string message, params object[] parameters)
        {
            AreNotEqual_(expected, actual, Format(message, parameters));
        }

        public static void AreNotEqual(double expected, double actual, double delta, string message)
        {
            var diff = Math.Abs(expected - actual);
            PostCheck(Asserter.CheckFalse(diff <= delta, message), ExceptionFactory, message);
        }

        public static void AreNotEqual(string expected, string actual, bool ignoreCase, string message, params object[] parameters)
        {
            AreNotEqual(expected, actual, ignoreCase, Format(message, parameters));
        }

        public static void AreNotEqual(double expected, double actual, double delta, string message, params object[] parameters)
        {
            AreNotEqual(expected, actual, delta, Format(message, parameters));
        }

        public static void AreNotEqual(float expected, float actual, float delta, string message, params object[] parameters)
        {
            AreNotEqual(expected, actual, delta, Format(message, parameters));
        }

        public static void AreNotEqual<T>(T expected, T actual)
        {
            AreNotEqual_(expected, actual, null);
        }

        public static void AreNotEqual<T>(T expected, T actual, string message)
        {
            AreNotEqual_(expected, actual, message);
        }

        public static void AreNotEqual<T>(T expected, T actual, string message, params object[] parameters)
        {
            AreNotEqual_(expected, actual, Format(message, parameters));
        }
    }
}
