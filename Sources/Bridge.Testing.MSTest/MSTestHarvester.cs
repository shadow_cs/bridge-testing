﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Reflection;
using Bridge.Testing.Helpers;

namespace Bridge.Testing.MSTest
{
    class MSTestCase : ITestCase
    {
        public Type Type { get; }
        private IEnumerable<ITestMethod> methods = null;

        public IEnumerable<ITestMethod> Methods { get
            {
                if (methods == null)
                    InitializeMethods();
                return methods;
            } }

        public MSTestCase(Type type)
        {
            this.Type = type;
        }

        private void InitializeMethods()
        {
            var result = new List<ITestMethod>();
            foreach (var method in Type.GetMethods())
            {
                if (method.IsPublic && method.HasAttribute<TestMethodAttribute>(false))
                    result.Add(new MSTestMethod(method));
            }
            methods = result;
        }

        public void Begin()
        {
        }

        public void End()
        {
        }
    }

    class MSTestMethod : TestMethodBase
    {
        public override MethodInfo Method { get; }

        public MSTestMethod(MethodInfo method)
        {
            this.Method = method;
        }

        public override void Run(IAsserter asserter)
        {
            using (new AssertBase.IsolatedAssert(asserter))
            {
                var inst = CreateInstance(Method.DeclaringType);
                Invoke(inst, Method);
            }
        }
    }

    public class MSTestHarvester : TestHarvester
    {
        public override IEnumerable<ITestCase> Harvest(Assembly assembly)
        {
            var result = new List<ITestCase>();
            foreach (var type in assembly.GetTypes())
            {
                if (IsEligibleType(type) && type.HasAttribute<TestClassAttribute>(false))
                    result.Add(new MSTestCase(type));
            };
            return result;
        }
    }
}