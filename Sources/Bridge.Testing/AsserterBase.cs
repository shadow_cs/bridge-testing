﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using System;

namespace Bridge.Testing
{
    public abstract class AsserterBase : IAsserter
    {
        public TestStatus Status { get; private set; }
        public bool CheckCalled { get { return Status != TestStatus.NoCheck; } }

        public void ResetStatus()
        {
            Status = TestStatus.NoCheck;
        }

        public abstract bool CheckTrue(bool value, string message);

        public abstract bool CheckFalse(bool value, string message);

        public abstract bool Equal(object expected, object actual, string message);

        public abstract void Fail(string message);

        public abstract bool NotEqual(object expected, object actual, string message);

        public abstract void Pass(string message);

        public abstract void Inconclusive(string message);

        public abstract bool Push(TestStatus result, object expected, object actual, string message);

        /// <summary>
        /// Returns true if given value is native JS type.
        /// If the function returns true it is safe to assume the value
        /// may be used with standard JS comparison operators (or passed)
        /// to non Bridge JS testing frameworks.
        /// </summary>
        /// <param name="value">Value to test</param>
        /// <returns>True if given value is native JS type.</returns>
        protected bool IsNative(object value)
        {
            if (value == null)
                return true;
            var type = value.GetType();
            // We're using internal Bridge API that works well in our case.
            // Classes, structs, arrays and (U)Int64 (which are not native types to JS)
            // define kind as either class or struct.
            // Strings and Boolean return native JS prototype for string which does not define $kind.
            // Numbers return "".
            return Script.Write<bool>("!type.$kind");
        }

        /// <summary>
        /// Compares the two values for equality.
        /// </summary>
        protected bool IsEqual(object expected, object actual)
        {
            Mark(TestStatus.Passed);
            return object.Equals(expected, actual);
        }

        protected virtual void Mark(TestStatus newStatus)
        {
            if (newStatus == TestStatus.NoCheck)
                throw new ArgumentException("NoCheck cannot be passed here");
            else if ((newStatus == TestStatus.Skipped) && (Status != TestStatus.NoCheck))
                throw new ArgumentException("Invalid use of skipped status");
            else if ((Status == TestStatus.Skipped) && (newStatus != TestStatus.Skipped))
                throw new ArgumentException("Cannot assign another status to an already skipped test");
            if (newStatus > Status)
                Status = newStatus;
        }

        protected void Mark(bool passed)
        {
            Mark(passed ? TestStatus.Passed : TestStatus.Failed);
        }
    }
}
