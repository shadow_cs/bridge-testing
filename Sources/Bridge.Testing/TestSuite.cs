﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using System;
using System.Collections.Generic;
using System.Reflection;

namespace Bridge.Testing
{
    /// <summary>
    /// Describes a class holding the test methods.
    /// </summary>
    public interface ITestCase
    {
        Type Type { get; }
        IEnumerable<ITestMethod> Methods { get; }

        /// <summary>
        /// Ran before any test methods are executed
        /// </summary>
        void Begin();

        /// <summary>
        /// Ran after all test methods are executed
        /// </summary>
        void End();
    }

    /// <summary>
    /// Describes a test method inside test case.
    /// </summary>
    public interface ITestMethod
    {
        MethodInfo Method { get; }
        void Run(IAsserter asserter);
    }

    /// <summary>
    /// Test harvesters collect the tests (usually from metadata).
    /// Implement this interface to create wrapper for existing .Net
    /// testing frameworks.
    /// </summary>
    public interface ITestHarvester
    {
        IEnumerable<ITestCase> Harvest(Assembly assembly);
    }

    /// <summary>
    /// Test runners are responsible for executing given tests.
    /// Implement this interface to create wrapper for existing JS
    /// JS testing framework (runner).
    /// </summary>
    public interface ITestRunner
    {
        void RunTests(IEnumerable<ITestCase> tests);
    }
}