﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using System;

namespace Bridge.Testing
{
    using System.Diagnostics;
    using ExceptionFactory = Func<string, TestStatusException>;

    /// <summary>
    /// Base class for implementing test checking logic.
    /// Subclasses create a wrapper between the runner custom checking
    /// logic (<see cref="IAsserter"/>) and the .Net testing framework
    /// checking logic.
    /// </summary>
    public class AssertBase
    {
        /// <summary>
        /// If set to true the test will continue even if some assertion
        /// fails. This is not the default behavior for most of .Net frameworks
        /// but some runners support it so there is no reason not to allow them to.
        /// </summary>
        public static bool ContinueOnFailure { get; set; } = false;

        /// <summary>
        /// Test runners must create <see cref="IsolatedAssert"/> prior running a test
        /// method and dispose it after the test method finished running.
        /// </summary>
        protected static IAsserter Asserter { get; private set; }

        /// <summary>
        /// Fails the assertion without checking any conditions.
        /// </summary>
        protected static void Fail(ExceptionFactory exceptionFactory, string message)
        {
            Asserter.Fail(message);
        }

        /// <summary>
        /// Fails the assertion without checking any conditions.
        /// </summary>
        protected static void Fail(ExceptionFactory exceptionFactory, string message,
            params object[] args)
        {
            Asserter.Fail(Format(message, args));
        }

        /// <summary>
        /// Makes sure <see cref="ContinueOnFailure"/> is satisfied. Should be called after
        /// each check with the result of the check.
        /// </summary>
        protected static void PostCheck(bool ok, ExceptionFactory exceptionFactory,
            string message)
        {
            // Since most .Net frameworks always throws some exception on failure
            // meaning the test will stop we have to comply with this philosophy
            // (if not disabled by the user).
            if ((!ok) && (!ContinueOnFailure))
                throw exceptionFactory(message);
        }

        /// <summary>
        /// Shorthand for <see cref="string.Format(string, object[])"/>.
        /// </summary>
        protected static string Format(string format, params object[] args)
        {
            return string.Format(format, args);
        }

        /// <summary>
        /// Enables use of the <see cref="AssertBase"/> class/subclasses
        /// and assigns its asserter while ensuring the assert is being
        /// used in complete isolation.
        /// </summary>
        public class IsolatedAssert : IDisposable
        {
            IAsserter asserter;

            public IsolatedAssert(IAsserter asserter)
            {
                if (asserter == null)
                    throw new ArgumentNullException(nameof(asserter));
                if (Asserter != null)
                    throw new Exception("Asserter isolation violated");
                this.asserter = Asserter = asserter;
            }

            public void Dispose()
            {
                if (Asserter != asserter)
                    throw new Exception("Asserter isolation violated");
                Asserter = null;
            }
        }

        /// <summary>
        /// Enables use of the <see cref="AssertBase"/> class/subclasses
        /// and assigns its asserter while enabling nested asserters
        /// (use with caution).
        /// </summary>
        public class NestedAssert : IDisposable
        {
            IAsserter asserter;
            IAsserter previous;

            public NestedAssert(IAsserter asserter)
            {
                if (asserter == null)
                    throw new ArgumentNullException(nameof(asserter));
                previous = Asserter;
                this.asserter = Asserter = asserter;
            }

            public void Dispose()
            {
                if (Asserter != asserter)
                    throw new Exception("Asserter isolation violated");
                Asserter = previous;
            }
        }

        /// <summary>
        /// Enables use of the <see cref="AssertBase"/> class/subclasses,
        /// all data sent to the asserters are discarded.
        /// </summary>
        public class NullAssert : NestedAssert
        {
            public NullAssert() : base(NullAsserter.Instance)
            {
            }
        }
    }
}