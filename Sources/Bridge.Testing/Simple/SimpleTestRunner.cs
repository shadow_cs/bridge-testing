﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Bridge.Html5;
using System;
using System.Collections.Generic;

namespace Bridge.Testing.Simple
{
    public class SimpleTestRunner: TestRunner
    {
        private static HTMLSpanElement escaper = null;
        private string Escape(string s)
        {
            if (escaper == null)
                escaper = new HTMLSpanElement();
            escaper.TextContent = s;
            s = escaper.InnerHTML;
            escaper.InnerHTML = "";
            return s;
        }

        private HTMLLIElement NewTestNode(string name, HTMLUListElement list)
        {
            var result = new HTMLLIElement() { InnerHTML = $"<code>{name}</code>" };
            list.AppendChild(result);
            return result;
        }

        private void FormatStatus(TestStatus status, out string summary, out string color)
        {
            switch (status)
            {
                case TestStatus.NoCheck:
                    summary = "OK - No check";
                    color = "DarkOrange";
                    break;
                case TestStatus.Skipped:
                    summary = "Skipped";
                    color = "Gray";
                    break;
                case TestStatus.Passed:
                    summary = "OK";
                    color = "green";
                    break;
                case TestStatus.Inconclusive:
                    summary = "Inconclusive";
                    color = "Gray";
                    break;
                case TestStatus.Failed:
                    summary = "FAILED";
                    color = "red";
                    break;
                case TestStatus.Error:
                    summary = "ERROR";
                    color = "red";
                    break;
                case TestStatus.Warning:
                    summary = "Warning";
                    color = "DarkOrange";
                    break;
                default:
                    throw new ArgumentException(nameof(status));
            }
        }

        private void FormatException(Exception e, out string summary, out string color)
        {
            summary = $"EXCEPTION - {e.GetType().Name}";
            color = "red";
            if ((e.Message != null) && (!"".Equals(e.Message)))
                summary += $": {Escape(e.Message)}";
        }

        private void HandleException(string name, Exception e, HTMLUListElement list)
        {
            string summary, color;
            FormatException(e, out summary, out color);
            WriteResult(NewTestNode(name, list), summary, color);
        }

        private void WriteResult(HTMLLIElement result, string summary, string color)
        {
            result.InnerHTML += $" ... <font color=\"{color}\">{summary}</font>";
        }

        private void RunTestMethod(ITestMethod test, SimpleAsserter asserter, HTMLUListElement list)
        {
            var result = NewTestNode(test.Method.Name, list);
            var summary = "OK";
            var color = "green";
            try
            {
                asserter.ResetStatus();
                test.Run(asserter);
                switch (asserter.Status)
                {
                    case TestStatus.NoCheck:
                    case TestStatus.Inconclusive:
                        FormatStatus(asserter.Status, out summary, out color);
                        break;
                }
            }
            catch (TestStatusException s)
            {
                FormatStatus(s.Status, out summary, out color);
                if ((s.Message != null) && (!"".Equals(s.Message)))
                    summary += $": {Escape(s.Message)}";
            }
            catch (Exception e)
            {
                FormatException(e, out summary, out color);
            }
            WriteResult(result, summary, color);
        }

        private void RunTestCase(ITestCase testCase, SimpleAsserter asserter)
        {
            Document.Body.AppendChild(new HTMLHeadingElement(HeadingType.H3)
            {
                InnerHTML = $"<code>{testCase.Type.FullName}</code>"
            });
            var list = new HTMLUListElement();
            Document.Body.AppendChild(list);

            try
            {
                // Test case set up
                try
                {
                    testCase.Begin();
                }
                catch (Exception e)
                {
                    HandleException("SetUp", e, list);
                    return;
                }
                // Run tests
                foreach (var test in testCase.Methods)
                {
                    // Should not throw exception
                    RunTestMethod(test, asserter, list);
                }
                // Test case tear down
            }
            finally
            {
                // Run tear down in case of set up exception
                try
                {
                    testCase.End();
                }
                catch (Exception e)
                {
                    HandleException("TearDown", e, list);
                }
            }
        }

        public override void RunTests(IEnumerable<ITestCase> tests)
        {
            var asserter = new SimpleAsserter();
            foreach (var testCase in tests)
            {
                // Should not throw exception
                RunTestCase(testCase, asserter);
            }
        }
    }
}
