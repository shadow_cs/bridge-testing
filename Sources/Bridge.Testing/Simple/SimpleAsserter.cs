﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using System;

namespace Bridge.Testing.Simple
{
    class FailedException: TestStatusException
    {
        public override TestStatus Status => TestStatus.Failed;

        public FailedException(string message) : base(message) { }
    }

    class SimpleAsserter : AsserterBase
    {
        public override bool CheckTrue(bool value, string message)
        {
            if (!value)
                Fail(message);
            else Mark(TestStatus.Passed);
            return value;
        }

        public override bool CheckFalse(bool value, string message)
        {
            return CheckTrue(!value, message);
        }

        public override bool Equal(object expected, object actual, string message)
        {
            return CheckTrue(IsEqual(expected, actual), message);
        }

        public override void Fail(string message)
        {
            Mark(TestStatus.Failed);
            throw new FailedException(message);
        }

        public override bool NotEqual(object expected, object actual, string message)
        {
            return CheckFalse(IsEqual(expected, actual), message);
        }

        public override void Pass(string message)
        {
            Mark(TestStatus.Passed);
        }

        public override void Inconclusive(string message)
        {
            Mark(TestStatus.Inconclusive);
        }

        public override bool Push(TestStatus result, object expected, object actual, string message)
        {
            Mark(result);
            return result == TestStatus.Passed;
        }
    }
}
