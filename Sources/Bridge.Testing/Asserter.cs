﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

namespace Bridge.Testing
{
    /// <summary>
    /// To fully plug in to JS test runners we cannot rely solely on exceptions,
    /// test runners must create a class implementing this interface and assign 
    /// it to <see cref="AssertBase.Asserter"/> to direct test checks done by the
    /// .Net testing framework wrappers to JS testing framework wrappers (the runner).
    /// </summary>
    public interface IAsserter
    {
        /// <summary>
        /// Indicates test failure
        /// </summary>
        void Fail(string message);

        /// <summary>
        /// Indicates passed test (marks that some assertion was executed).
        /// </summary>
        void Pass(string message);

        /// <summary>
        /// Indicates that the test method was unable to tell whether the test
        /// succeeded or not.
        /// </summary>
        void Inconclusive(string message);

        bool CheckTrue(bool value, string message);

        bool CheckFalse(bool value, string message);

        bool Equal(object expected, object actual, string message);

        bool NotEqual(object expected, object actual, string message);

        bool Push(TestStatus result, object expected, object actual, string message);
    }
}