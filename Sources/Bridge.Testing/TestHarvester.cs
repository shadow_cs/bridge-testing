﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using System;
using System.Collections.Generic;
using System.Reflection;

namespace Bridge.Testing
{
    /// <summary>
    /// Base class for <see cref="ITestHarvester"/> with some common functionality.
    /// </summary>
    public abstract class TestHarvester : ITestHarvester
    {
        public abstract IEnumerable<ITestCase> Harvest(Assembly assembly);

        protected bool IsEligibleType(Type type)
        {
            return type.IsPublic // Metadata/reflection generation must be enabled
                && type.IsClass;
        }
    }
}
