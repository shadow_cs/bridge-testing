﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

namespace Bridge.Testing
{
    /// <summary>
    /// Stub asserter implementation
    /// </summary>
    public class NullAsserter : IAsserter
    {
        private static NullAsserter instance;

        public static NullAsserter Instance
        {
            get
            {
                if (instance == null)
                    instance = new NullAsserter();
                return instance;
            }
        }

        public bool CheckFalse(bool value, string message)
        {
            return true;
        }

        public bool CheckTrue(bool value, string message)
        {
            return true;
        }

        public bool Equal(object expected, object actual, string message)
        {
            return true;
        }

        public void Fail(string message)
        {
        }

        public void Inconclusive(string message)
        {
        }

        public bool NotEqual(object expected, object actual, string message)
        {
            return true;
        }

        public void Pass(string message)
        {
        }

        public bool Push(TestStatus result, object expected, object actual, string message)
        {
            return true;
        }
    }
}
