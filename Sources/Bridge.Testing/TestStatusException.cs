﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using System;

namespace Bridge.Testing
{
    /// <summary>
    /// Used by some asserters/frameworks (not mandatory) to mark test case failures.
    /// </summary>
    public abstract class TestStatusException : Exception
    {
        public override string Message { get; }

        public abstract TestStatus Status { get; }

        protected TestStatusException() : base(null)
        {
        }

        public TestStatusException(string message) : base(message)
        {
            // Custom message is assigned in constructor if message is null
            // but we want null to mark a failure without any message.
            Message = message;
        }

        public TestStatusException(string message, Exception inner) : base(message, inner)
        {
            // See above
            Message = message;
        }
    }
}
