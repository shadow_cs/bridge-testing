﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bridge.Testing.Tests.MSTest
{
    public partial class AssertTests
    {
        [TestMethod]
        public void IsTrue()
        {
            Assert.IsTrue(true, FAIL);
            CheckFailure(() => { Assert.IsTrue(false, OK); });
            Assert.IsTrue(true);
            CheckFormatted(() => { Assert.IsTrue(false, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void IsFalse()
        {
            Assert.IsFalse(false, FAIL);
            CheckFailure(() => { Assert.IsFalse(true, OK); });
            Assert.IsFalse(false);
            CheckFormatted(() => { Assert.IsFalse(true, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void IsInstanceOfType()
        {
            var o = new object();
            var foo = new Foo();
            Assert.IsInstanceOfType(foo, typeof(Foo), FAIL);
            Assert.IsInstanceOfType(foo, typeof(IFoo), FAIL);
            CheckFailure(() => { Assert.IsInstanceOfType(o, typeof(Foo), OK); });
            CheckFailure(() => { Assert.IsInstanceOfType(null, typeof(Foo), OK); });
            Assert.IsInstanceOfType(o, typeof(object));
            CheckFormatted(() => { Assert.IsInstanceOfType(o, typeof(IFoo), FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void IsNotInstanceOfType()
        {
            var o = new object();
            var foo = new Foo();
            Assert.IsNotInstanceOfType(o, typeof(Foo), FAIL);
            CheckFailure(() => { Assert.IsNotInstanceOfType(foo, typeof(Foo), OK); });
            CheckFailure(() => { Assert.IsNotInstanceOfType(null, typeof(Foo), OK); });
            CheckFailure(() => { Assert.IsNotInstanceOfType(o, typeof(object), OK); });
            Assert.IsNotInstanceOfType(o, typeof(IFoo));
            CheckFormatted(() => { Assert.IsNotInstanceOfType(foo, typeof(IFoo), FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void IsNotNull()
        {
            Assert.IsNotNull("", FAIL);
            CheckFailure(() => { Assert.IsNotNull(null, OK); });
            Assert.IsNotNull("");
            CheckFormatted(() => { Assert.IsNotNull(null, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void IsNull()
        {
            Assert.IsNull(null, FAIL);
            CheckFailure(() => { Assert.IsNull("", OK); });
            Assert.IsNull(null);
            CheckFormatted(() => { Assert.IsNull("", FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }
    }
}
