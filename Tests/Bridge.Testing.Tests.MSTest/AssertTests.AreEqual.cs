﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Bridge.Testing.Tests.MSTest
{
    public partial class AssertTests
    {
        [TestMethod]
        public void AreEqual_Object()
        {
            object o1 = new Value<int>(42);
            object o2 = new Value<int>(42);
            Assert.AreEqual(o1, o2, FAIL);
            o2 = new Value<int>(-42);
            CheckFailure(() => { Assert.AreEqual(o1, o2, OK); });
            o2 = new Value<int>(42);
            Assert.AreEqual(o1, o2);
            o2 = new Value<int>(-42);
            CheckFormatted(() => { Assert.AreEqual(o1, o2, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void AreEqual_String()
        {
            var s = String.Format("{0}{1}", "equ", "als"); // Make new reference
            Assert.AreEqual("equals", s, FAIL);
            CheckFailure(() => { Assert.AreEqual("equals", "not equals", OK); });
            Assert.AreEqual("equals", s);
            CheckFormatted(() => { Assert.AreEqual("Equals", s, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void AreEqual_Int()
        {
            Assert.AreEqual(42, 42, FAIL);
            CheckFailure(() => { Assert.AreEqual(42, -42, OK); });
            Assert.AreEqual(42, 42);
            CheckFormatted(() => { Assert.AreEqual(42, -42, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void AreEqual_Double()
        {
            Assert.AreEqual(3.14, 3.14, FAIL);
            CheckFailure(() => { Assert.AreEqual(3.14, -3.14, OK); });
            Assert.AreEqual(3.14, 3.14);
            CheckFormatted(() => { Assert.AreEqual(3.14, -3.14, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void AreEqual_Bool()
        {
            Assert.AreEqual(true, true, FAIL);
            CheckFailure(() => { Assert.AreEqual(true, false, OK); });
            Assert.AreEqual(true, true);
            CheckFormatted(() => { Assert.AreEqual(false, true, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void AreEqual_String_IgnoreCase()
        {
            var s = String.Format("{0}{1}", "equ", "als"); // Make new reference
            Assert.AreEqual("equals", s, false, FAIL);
            CheckFailure(() => { Assert.AreEqual("equals", "Equals", false, OK); });
            CheckFailure(() => { Assert.AreEqual("equals", "equalss", false, OK); });
            s = String.Format("{0}{1}", "Equ", "als"); // Make new reference
            Assert.AreEqual("equAls", s, true, FAIL);
            Assert.AreEqual("Equals", s, true);
            CheckFormatted(() => { Assert.AreEqual("eQualss", s, true, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void AreEqual_Double_Delta()
        {
            Assert.AreEqual(0.1, 0, 0.1, FAIL);
            CheckFailure(() => { Assert.AreEqual(0.1, -0.0000000001, 0.1, OK); });
            Assert.AreEqual(0.05, -0.05, 0.1);
            CheckFormatted(() => { Assert.AreEqual(0, 1, 0.999999, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void AreEqual_Float_Delta()
        {
            Assert.AreEqual((float)0.1, (float)0, (float)0.1, FAIL);
            CheckFailure(() => { Assert.AreEqual((float)0.1, (float)-0.0000000001, (float)0.1, OK); });
            Assert.AreEqual((float)0.05, (float)-0.05, (float)0.1);
            CheckFormatted(() => { Assert.AreEqual((float)0, (float)1, (float)0.999999, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }
    }
}
