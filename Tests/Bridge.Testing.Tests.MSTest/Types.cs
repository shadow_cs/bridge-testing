﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

namespace Bridge.Testing.Tests.MSTest
{
    class Value<T>
    {
        public T V { get; set; }

        public Value(T v)
        {
            V = v;
        }

        public override bool Equals(object o)
        {
            if (o == null)
                return false;
            var v = o as Value<T>;
            if (v != null)
                return Equals(v.V, V);
            else return base.Equals(o);
        }

        public override int GetHashCode()
        {
            return V.GetHashCode();
        }
    }

    interface IFoo
    {

    }

    class Foo: IFoo
    {

    }
}
