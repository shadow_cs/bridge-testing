﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Bridge.Testing.Tests.MSTest
{
    class TestFailedException : TestStatusException
    {
        public override TestStatus Status => TestStatus.Failed;

        public TestFailedException(string msg) : base(msg) { }
    }

    [TestClass, Reflectable]
    public partial class AssertTests
    {
        const string FAIL = "$FAILED$";
        const string OK = "$OK$";
        const string COMPLETE = "This test method completed OK";
        const string FMT = "${0}$";
        const string MSG = "$TEST$";

        private TestStatusException CheckException(Action action)
        {
            try
            {
                action();
            }
            catch (TestStatusException e)
            {
                return e;
            }
            throw new TestFailedException("Previous check should've failed");
        }

        private void CheckFailure(Action action)
        {
            if (!OK.Equals(CheckException(action).Message))
                throw new TestFailedException("Invalid exception message");
        }

        private void CheckFormatted(Action action)
        {
            if (!"$$TEST$$".Equals(CheckException(action).Message))
                throw new TestFailedException("Invalid formatted exception message");
        }
    }
}
