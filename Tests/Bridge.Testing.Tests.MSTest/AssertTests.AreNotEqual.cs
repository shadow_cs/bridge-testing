﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Bridge.Testing.Tests.MSTest
{
    public partial class AssertTests
    {
        [TestMethod]
        public void AreNotEqual_Object()
        {
            object o1 = new Value<int>(42);
            object o2 = new Value<int>(-42);
            Assert.AreNotEqual(o1, o2, FAIL);
            o2 = new Value<int>(42);
            CheckFailure(() => { Assert.AreNotEqual(o1, o2, OK); });
            o2 = new Value<int>(-42);
            Assert.AreNotEqual(o1, o2);
            o2 = new Value<int>(42);
            CheckFormatted(() => { Assert.AreNotEqual(o1, o2, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void AreNotEqual_String()
        {
            var s = String.Format("{0}{1}", "equ", "als"); // Make new reference
            Assert.AreNotEqual("Equals", s, FAIL);
            CheckFailure(() => { Assert.AreNotEqual("equals", "equals", OK); });
            Assert.AreNotEqual("equalss", s);
            CheckFormatted(() => { Assert.AreNotEqual("equals", s, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void AreNotEqual_Int()
        {
            Assert.AreNotEqual(42, -42, FAIL);
            CheckFailure(() => { Assert.AreNotEqual(42, 42, OK); });
            Assert.AreNotEqual(42, -42);
            CheckFormatted(() => { Assert.AreNotEqual(42, 42, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void AreNotEqual_Double()
        {
            Assert.AreNotEqual(3.14, -3.14, FAIL);
            CheckFailure(() => { Assert.AreNotEqual(3.14, 3.14, OK); });
            Assert.AreNotEqual(3.14, -3.14);
            CheckFormatted(() => { Assert.AreNotEqual(3.14, 3.14, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void AreNotEqual_Bool()
        {
            Assert.AreNotEqual(true, false, FAIL);
            CheckFailure(() => { Assert.AreNotEqual(true, true, OK); });
            Assert.AreNotEqual(false, true);
            CheckFormatted(() => { Assert.AreNotEqual(false, false, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void AreNotEqual_String_IgnoreCase()
        {
            var s = String.Format("{0}{1}", "equ", "als"); // Make new reference
            Assert.AreNotEqual("Equals", s, false, FAIL);
            CheckFailure(() => { Assert.AreNotEqual("equals", "equals", false, OK); });
            CheckFailure(() => { Assert.AreNotEqual("equals", "Equals", true, OK); });
            s = String.Format("{0}{1}", "Equ", "als"); // Make new reference
            Assert.AreNotEqual("equAlss", s, true, FAIL);
            Assert.AreNotEqual("eEquals", s, true);
            CheckFormatted(() => { Assert.AreNotEqual("eQuals", s, true, FMT, MSG); });
            Assert.AreNotEqual("eQuals ", s, true, FMT, MSG);
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void AreNotEqual_Double_Delta()
        {
            Assert.AreNotEqual(0.1, -0.0000000001, 0.1, FAIL);
            CheckFailure(() => { Assert.AreNotEqual(0.1, 0, 0.1, OK); });
            Assert.AreNotEqual(0, 1, 0.999999);
            CheckFormatted(() => { Assert.AreNotEqual(0.05, -0.05, 0.1, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void AreNotEqual_Float_Delta()
        {
            Assert.AreNotEqual((float)0.1, (float)-0.0000000001, (float)0.1, FAIL);
            CheckFailure(() => { Assert.AreNotEqual((float)0.1, (float)0, (float)0.1, OK); });
            Assert.AreNotEqual((float)0, (float)1, (float)0.999999);
            CheckFormatted(() => { Assert.AreNotEqual((float)0.05, (float)-0.05, (float)0.1, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }
    }
}
