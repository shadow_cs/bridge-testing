﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Bridge.Testing.Tests.MSTest
{
    public partial class AssertTests
    {
        [TestMethod]
        public void ThrowsException_Action()
        {
            Exception e = new ArgumentException();
            Action action = () => { throw e; };
            Exception result = Assert.ThrowsException<Exception>(action, FAIL);
            Assert.AreSame(e, result);

            action = () => { };
            CheckFailure(() => { Assert.ThrowsException<Exception>(action, OK); });

            e = new ArgumentException();
            action = () => { throw e; };
            result = Assert.ThrowsException<ArgumentException>(action);
            Assert.AreSame(e, result);

            action = () => { throw new ArgumentException(); };
            CheckFormatted(() => { Assert.ThrowsException<ArithmeticException>(action, FMT, MSG); });

            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void ThrowsException_Func()
        {
            Exception e = new ArgumentException();
            Func<object> action = () => { throw e; };
            Exception result = Assert.ThrowsException<Exception>(action, FAIL);
            Assert.AreSame(e, result);

            action = () => null;
            CheckFailure(() => { Assert.ThrowsException<Exception>(action, OK); });

            e = new ArgumentException();
            action = () => { throw e; };
            result = Assert.ThrowsException<ArgumentException>(action);
            Assert.AreSame(e, result);

            action = () => { throw new ArgumentException(); };
            CheckFormatted(() => { Assert.ThrowsException<ArithmeticException>(action, FMT, MSG); });

            Assert.IsTrue(true, COMPLETE);
        }
    }
}