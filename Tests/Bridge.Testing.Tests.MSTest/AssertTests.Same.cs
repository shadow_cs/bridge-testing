﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bridge.Testing.Tests.MSTest
{
    public partial class AssertTests
    {
        [TestMethod]
        public void AreSame()
        {
            object same = new object();
            object notSame = new object();
            Assert.AreSame(same, same, FAIL);
            CheckFailure(() => { Assert.AreSame(same, notSame, OK); });
            Assert.AreSame(same, same);
            CheckFormatted(() => { Assert.AreSame(notSame, same, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void AreNotSame()
        {
            object same = new object();
            object notSame = new object();
            Assert.AreNotSame(same, notSame, FAIL);
            CheckFailure(() => { Assert.AreNotSame(same, same, OK); });
            Assert.AreNotSame(notSame, same);
            CheckFormatted(() => { Assert.AreNotSame(same, same, FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }
    }
}
