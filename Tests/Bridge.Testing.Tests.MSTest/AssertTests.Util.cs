﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bridge.Testing.Tests.MSTest
{
    public partial class AssertTests
    {
        [TestMethod]
        public void Fail()
        {
            CheckException(Assert.Fail);
            CheckFailure(() => { Assert.Fail(OK); });
            CheckFormatted(() => { Assert.Fail(FMT, MSG); });
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void Inconclusive()
        {
            Assert.Inconclusive();
            Assert.Inconclusive(OK);
            Assert.Inconclusive(FMT, MSG);
            Assert.IsTrue(true, COMPLETE);
        }

        [TestMethod]
        public void ReplaceNullChars()
        {
            Assert.AreEqual("Test \\0\\0", Assert.ReplaceNullChars("Test \0\0"));
            Assert.IsTrue(true, COMPLETE);
        }
    }
}
