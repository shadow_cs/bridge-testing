﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using System;
using System.Text.RegularExpressions;

namespace System.IO
{
    public class StringReader
    {
        private static readonly Regex re = new Regex("\\r|\\n|\\r\\n");

        private int position = 0;
        private readonly string buffer;

        public StringReader(string buffer)
        {
            if (buffer == null)
                throw new ArgumentNullException(nameof(buffer));
            this.buffer = buffer;
        }

        public string ReadLine()
        {
            if (position >= buffer.Length)
                return null;
            var match = re.Match(buffer, position);
            if (match.Success)
            {
                var result = buffer.Substr(position, match.Index - position);
                position = match.Index + match.Length;
                return result;
            }
            else
            {
                var result = buffer.Substr(position);
                position = buffer.Length;
                return result;
            }
        }
    }
}
