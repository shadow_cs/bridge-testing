﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Bridge.Testing;
using Bridge.Testing.NUnit;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NUnit.Framework
{
    public class TestSuite: NUnit.Framework.Internal.TestSuite
    {
        private List<NUnitTestCase> fixtures = new List<NUnitTestCase>();

        internal IEnumerable<NUnitTestCase> Fixtures => fixtures;

        public TestSuite(string name): base("", name) { }

        public void Add(NUnitTestCase fixture)
        {
            fixtures.Add(fixture);
        }
    }
    public class TestBuilder
    {
        private static void RunInternal(NUnitTestCase testCase)
        {
            try
            {
                try
                {
                    testCase.Begin();
                    foreach (NUnitTestMethod testMethod in testCase.Methods)
                    {
                        try
                        {
                            testMethod.RunInternal(NullAsserter.Instance, false);
                        }
                        catch
                        {
                            // Recorded in the test method result
                        }
                    }
                }
                finally
                {
                    // Run tear down in case of set up exception
                    testCase.End();
                }
            }
            catch 
            {
                // Recorded in the suite result
            }
        }

        public static ITestResult RunTestCase(Type fixture, string method)
        {
            var testCase = new NUnitTestCase(fixture);
            testCase.AddMethod(method);
            RunInternal(testCase);
            var result = testCase.LastResult;
            return result.HasChildren ? result.Children.First() : result;
            
        }

        public static NUnitTestCase MakeFixture(Type fixture)
        {
            return new NUnitTestCase(fixture);
        }

        public static ITestResult RunTestFixture(Type fixture)
        {
            var testCase = new NUnitTestCase(fixture);
            RunInternal(testCase);
            return testCase.LastResult;
        }

        public static ITestResult RunTestFixture(object fixture)
        {
            var testCase = new NUnitTestCase(fixture);
            RunInternal(testCase);
            return testCase.LastResult;
        }

        public static ITestResult RunTest(TestSuite suite)
        {
            TestSuiteResult result = (TestSuiteResult)suite.MakeTestResult();
            foreach (var child in suite.Fixtures)
            {
                // Should not throw exception.
                RunInternal(child);
                result.AddResult(child.LastResult);
            }
            return result;
        }
    }
}
