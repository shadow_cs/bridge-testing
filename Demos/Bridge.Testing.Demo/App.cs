﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Bridge.Html5;
using Bridge.Testing.Simple;
using Bridge.Testing.MSTest;
using Bridge.Testing.QUnit;
using System.Reflection;
using Bridge.Testing.NUnit;

namespace Bridge.Testing.Demo
{
    // Limit metadata generation
    [Reflectable(false)]
    public class App
    {
        public static void Main()
        {
            var args = Window.Location.Search;

            if (args.Contains("qunit"))
            {
                // Most of the time, this should be the only line you should need to put to your main.
                QUnitRunner.Instance.RunTests(new MSTestHarvester().Harvest(Assembly.GetExecutingAssembly()));
                return;
            }

            var button = new HTMLButtonElement
            {
                InnerHTML = "Run MSTest, Simple runner",
                OnClick = e => new SimpleTestRunner().RunTests(new MSTestHarvester().Harvest(Assembly.GetExecutingAssembly()))
            };
            Document.Body.AppendChild(button);

            button = new HTMLButtonElement
            {
                InnerHTML = "Run NUnit, Simple runner",
                OnClick = e => new SimpleTestRunner().RunTests(new NUnitHarvester().Harvest(Assembly.GetExecutingAssembly()))
            };
            Document.Body.AppendChild(button);

            var check = new HTMLInputElement()
            {
                Type = InputType.Checkbox,
                OnChange = e => AssertBase.ContinueOnFailure = ((HTMLInputElement)e.Target).Checked
            };
            Document.Body.AppendChild(check);
            Document.Body.AppendChild(new HTMLLabelElement() { TextContent = "Continue on failure" });

            var btnMSQ = new HTMLButtonElement { InnerHTML = "Run MSTest, QUnit" };
            var btnNUQ = new HTMLButtonElement { InnerHTML = "Run NUnit, QUnit" };
            Document.Body.AppendChild(btnMSQ);
            Document.Body.AppendChild(btnNUQ);
            btnMSQ.OnClick = e =>
            {
                btnMSQ.Disabled = true;
                btnNUQ.Disabled = true;
                QUnitRunner.Instance.RunTests(new MSTestHarvester().Harvest(Assembly.GetExecutingAssembly()));
            };
            btnNUQ.OnClick = e =>
            {
                btnMSQ.Disabled = true;
                btnNUQ.Disabled = true;
                QUnitRunner.Instance.RunTests(new NUnitHarvester().Harvest(Assembly.GetExecutingAssembly()));
            };

            // Since we're running the tests after pressing the button, we need to disable autostart
            Bridge.QUnit.QUnit.Config.Autostart = false;
        }
    }
}