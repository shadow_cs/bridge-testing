﻿//******************************************************************************
//
//           Bridge Testing Framework
//
//           Copyright (c) 2017 Honza Rames
//                                                                            
//           https://bitbucket.org/shadow_cs/bridge-testing
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//           http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//******************************************************************************

using Bridge.Testing.Demo.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Bridge.Testing.Demo.MSTest
{
    [TestClass]
    public class MSTestCase
    {
        [TestMethod]
        public void ShouldFail()
        {
            var sut = new ClassToTest();
            Assert.AreEqual(true, sut.Foo(null));
            Assert.AreEqual(true, true);
        }

        [TestMethod]
        public void ShouldRaiseException()
        {
            throw new Exception();
        }

        [TestMethod]
        public void ShouldSucceed()
        {
            Assert.AreEqual(0, 0);
        }

        [TestMethod]
        public void NoAssert()
        {
            
        }
    }
}
