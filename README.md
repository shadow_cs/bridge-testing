# Bridge Testing

Bridge Testing is a [Bridge.NET](http://bridge.net) extension that allows using
existing .Net testing frameworks with Bridge and JavaScript testing framework
(runners).

The framework is in proof of concept state but MSTest should already work for
simple tests that use only the attributes and `Assert` class. NUnit also
works for most common use cases (`TestCase` and data sources are not supported)
see the tests for supported functionality.

## Usage

Currently there is no NuGet package, you have to add required projects to your
solution.

Projects:

 * `Bridge.Testing` - common functionality and simple test runner
 * `Bridge.Testing.MSTest` - MSTest wrappers
 * `Bridge.Testing.NUnit` - NUnit wrappers
 * `Bridge.Testing.QUnit` - QUnit runner

## Example

Following example demonstrates usage of MSTest and QUnit runner with Bridge
(NUnit works in a similar fashion just use `NUnitHarvester`).

Prepare your solution:

 * Crate Bridge.NET project
 * Add `Bridge.Testing`, `Bridge.Testing.MSTest`, 
  `Bridge.Testing.QUnit` to your solution and add it as the test project 
  reference
 * Create the test class
```csharp
using System;
using Bridge;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Namespace
{
    [TestClass, Reflectable]
    public class TestClass
    {
        [TestMethod]
        public void Test()
        {
            Assert.IsTrue(true);
        }
    }
}
```
 * Create entry point class
```csharp
using System.Reflection;
using Bridge.Testing.MSTest;
using Bridge.Testing.QUnit;

namespace Namespace
{
    public class App
    {
        public static void Main()
        {
            var tests = new MSTestHarvester().Harvest(Assembly.GetExecutingAssembly());
            QUnitRunner.Instance.RunTests(tests);
        }
    }
}
```
 * Update `demo.html`, add following lines prior your application `js` (you have to use QUnit 2)
```html
    <link rel="stylesheet" href="http://code.jquery.com/qunit/qunit-2.1.1.css">
    <script src="http://code.jquery.com/qunit/qunit-2.1.1.js"></script>
    <script src="../output/bridge.testing.js"></script>
    <script src="../output/bridge.testing.mstest.js"></script>
    <script src="../output/bridge.testing.qunit.js"></script>
``` 
 * Open the web page to run the tests

![Test output](Assets/example.png)

## License

**Apache License, Version 2.0**

Please see [LICENSE](LICENSE) for details.